#!/bin/bash
# shellcheck disable=SC2046

#################### --------------#######################
####### Author: Israel Elías
#Add apigee User
sudo useradd apigee

#Install git
#This command is necesary to pull out all the commands and yo be updated
sudo yum install -y git
#Installation for vim, easier interface
sudo yum install -y vim
#Install Envoy repository
sudo yum install yum-utils
sudo rpm --import 'https://rpm.dl.getenvoy.io/public/gpg.CF716AF503183491.key'
curl -sL 'https://rpm.dl.getenvoy.io/public/config.rpm.txt?distro=el&codename=7' > /tmp/tetrate-getenvoy-rpm-stable.repo
sudo yum-config-manager --add-repo '/tmp/tetrate-getenvoy-rpm-stable.repo'
sudo yum makecache --disablerepo='*' --enablerepo='tetrate-getenvoy-rpm-stable'
sudo yum install getenvoy-envoy



#Install docker
#sudo yum-config-manager \
#    --add-repo \
#    https://download.docker.com/linux/rhel/docker-ce.repo


#Get envoy latest version, optional
#docker cp $(docker create envoyproxy/envoy:latest):/usr/local/bin/envoy .
#install jq and yq