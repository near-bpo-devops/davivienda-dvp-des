#!/bin/bash
# shellcheck disable=SC2046

ulimit -n {{ pillar.get('envoy_max_open_files', '102400') }}
sysctl fs.inotify.max_user_watches={{ pillar.get('envoy_max_inotify_watches', '524288') }}

exec /usr/bin/envoy -c /opt/apigee/etc/envoy/envoy.cfg --restart-epoch ${RESTART_EPOCH} --service-cluster {{ grains['apigee_envoy_dvp_cl_d01'] }} --service-node {{ grains['apigee_dvp_node_d01'] }} 

#--service-zone {{ grains.get('ec2_availability-zone', 'unknown') }}

# node:
#   cluster: apisdes_davivienda
#   id: apisdes-davivienda-id
