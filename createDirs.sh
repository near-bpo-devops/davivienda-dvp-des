#!/bin/bash

APIGEE_BASE_PATH=${1}


#Create based directories
mkdir -p "${APIGEE_BASE_PATH}/bin"
mkdir -p "${APIGEE_BASE_PATH}/etc"
mkdir -p "${APIGEE_BASE_PATH}/data"
mkdir -p "${APIGEE_BASE_PATH}/var"

#Create envoy directories
mkdir -p "${APIGEE_BASE_PATH}/bin/envoy"
mkdir -p "${APIGEE_BASE_PATH}/etc/envoy/certificates/server"
mkdir -p "${APIGEE_BASE_PATH}/etc/envoy/certificates/client"
mkdir -p "${APIGEE_BASE_PATH}/var/log/envoy"